<?php
namespace Artefacts\Woc\Tests ;

use PHPUnit\Framework\TestCase ;
use Artefacts\Woc\OAuth ;

class OAuthTest extends TestCase
{
	
	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasic()
	{
		$data = [
			'key1' => 'ABC',
			'key2' => [
				'key3' => 'DEF',
			],
		];
		$this->assertEquals( 'ABC', OAuth::array_getKeyPath( $data, 'key1' )  );
		$this->assertEquals( 'DEF', OAuth::array_getKeyPath( $data, 'key2.key3' )  );
		$this->assertNull( OAuth::array_getKeyPath( $data, 'key4' )  );
		$this->assertNull( OAuth::array_getKeyPath( $data, 'key5.key6' )  );

		$this->assertNull( OAuth::array_getKeyPath( $data, null )  );
		//$data = null ;
		//$this->assertNull( OAuth::array_getKeyPath( $data, 'none' )  );
	}
}
