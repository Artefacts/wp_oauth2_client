# wp_oauth2_client

Login into wordpress from another identity provider using OAuth2 protocol ...

Tested provider:
- Nextcloud
- LinkedIn

No Pro feature, all free & open :-)

## Installation

Just copy the plugin at the right place, like all Wordpress plugins.

The `composer.json` is only used for development, no need to care about it to install the plugin.

## Usage

Add SSO Login button to the Wordpress login form.

![SSO Login button in Login form](doc/woc_oauth_login_form.png)

Shortcode:
- `[woc_oauth_link]` draw a html "&lt;a&gt;" that link to sso login.
- `[woc_oauth_url]` return the url link to sso login.
  - Start login with the http query string argument "woc-oauth-login" like `https://wordpress.devhost/?woc-oauth-login`.

There are some options:
- enable automatic creation of user, if successed login on identity server.
- force normal user to only login via SSO. Normal login will be reserved to super admin.
- mapping created user role with remote user info.

### Providers

Documentation:
- [Nextcloud](doc/providers/nextcloud.md)
- [LinkedIn](doc/providers/linkedin.md)

## Tech

- jquery-ui, v1.13.0, theme "Humanity"

- files in `docs/` folder.
  - screenshots
  - providers: some technical notes about oauth providers

