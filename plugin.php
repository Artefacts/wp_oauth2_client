<?php
/**
 * Plugin Name: Wordpress OAuth2 Client
 * Version: 1.0.0
 * Text Domain: woc-oauth
 * Domain Path: /languages
 * Plugin URI: https://framagit.org/Artefacts/wp_oauth2_client
 * Description: Login into wordpress from another identity provider like Nextcloud or ... No Pro features, all free & open :-)
 * Requires at least: 5.8
 * Requires PHP: 7.3
 * Author: Cyrille37
 * License: GPL v2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Update URI: https://framagit.org/Artefacts/wp_oauth2_client
 */

require_once(__DIR__.'/src/WocPlugin.php');

use Artefacts\Woc\WocPlugin ;

WocPlugin::getInstance();
