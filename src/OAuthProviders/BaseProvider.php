<?php
namespace Artefacts\Woc\OAuthProviders ;

use Artefacts\Woc\OAuth ;
use Artefacts\Woc\Settings;
use Artefacts\Woc\WocPlugin;

/**
 * Doc
 * - WP http request https://developer.wordpress.org/reference/classes/WP_Http/request/
 */
class BaseProvider 
{
    const SESSION_KEY_STATE = 'woc-sso_state';

    const OAUTH_GRANT_TYPE = 'authorization_code';

    /**
     * HTTP request seconds timeout.
     * Keep a long time, like sometimes some providers will add wait time for security reason.
     */
    const HTTP_TIMEOUT = 60 ;
    /**
     * HTTP request max redirection.
     */
    const HTTP_REDIR = 3 ;

    public function __construct()
    {
        //Caf37Plugin::debug(__METHOD__);
    }

    public function callback_uri( )
    {
        return '/oauth' ;
    }

    /**
     * OAuth client url callback to retrieve the access token.
     * @return string
     */
    public function callback_url( )
    {
        static $callback_url ;
        if( ! $callback_url )
            $callback_url = home_url() . $this->callback_uri() . '/' ;
        return $callback_url ;
    }

    protected static $optionsConfig = [
        '_name'=> 'custom',
        //'oauth_url' => [],
        //'url_authorize' => [],
        //'url_token' => [],
        'client_id' => [
            // default behavior 'required' => ,
        ],
        'client_secret' => [
            // default behavior 'required' => ,
        ],
    ];

    public static function optionsConfig( Array $options = null )
    {
        if( ! empty($options))
        {
            return array_merge(self::$optionsConfig, $options);
        }
        return self::$optionsConfig ;
    }

    /**
     * Retrieve previous state $has saved in session and compare with $state.
     * Remove $hash from session.
     */
    public function checkAndForgetState( $state )
    {
        $hash = WocPlugin::get_session( self::SESSION_KEY_STATE );
        WocPlugin::set_session( self::SESSION_KEY_STATE );
        if( ! $hash )
            return false ;
        if( $hash != wp_hash( $state, 'nonce' ) )
        {
            return false ;
        }
        return true ;
    }

    protected function get_option_or_fail( $key )
    {
        $value = trim(Settings::get_option($key));
        if( empty($value) )
            throw new \InvalidArgumentException( $key.' required');
        return $value ;
    }

    /**
     * After authorization successed, we ask an Access Token to the OAuth server.
     */
    public function oauth_access_token( $authorization_token )
    {
        $url_base = $this->get_option_or_fail('oauth_url');
        $url = $url_base . $this->get_option_or_fail('url_token');
        $client_id = $this->get_option_or_fail('client_id');
        $client_secret = $this->get_option_or_fail('client_secret');

        // The OAuth server will validate this hash for request authorization.
        $http_auth_header = 'Basic ' . base64_encode( $client_id . ':' . $client_secret );

        // Default is "true". Set to "false" if wordpress debug.
        $sslverify = (defined('WP_DEBUG') && constant('WP_DEBUG') ? false : true ) ;

        $body = [
            'grant_type'    => self::OAUTH_GRANT_TYPE,
            // Client ID is not mandatoty, the authorization code is enough.
            'code'          => $authorization_token,
            // Some providers want (again) the Redirect url
            //'redirect_uri'  => self::callback_url().'?coucou=123',
            'redirect_uri'  => $this->callback_url(),
        ];

        $headers = [
            'Accept'  => 'application/json',
            'charset'       => 'UTF - 8',
            'Authorization' => $http_auth_header,
            'Content-Type' => 'application/x-www-form-urlencoded',
        ];
        //WocPlugin::debug(__METHOD__, 'headers', $headers );

        $cookies = [];

        $response = wp_remote_post($url, array(
            'method'      => 'POST',
            'timeout'     => self::HTTP_TIMEOUT,
            'redirection' => self::HTTP_REDIR,
            'httpversion' => '1.0',
            'blocking'    => true,
            'headers'     => $headers,
            'body'        => $body,
            'cookies'     => $cookies,
            'sslverify'   => $sslverify
        ));
        //WocPlugin::debug(__METHOD__, 'response', $response );

        if( is_wp_error($response) )
        {
            //$this->display_error( $response );
            return $response ;
        }

        //WocPlugin::debug(__METHOD__, 'response[response]:', $response['response'] );
        if( $response['response']['code'] != 200 )
        {
            throw new \RuntimeException('Http Post failed: '.json_encode($response['response']) );
        }

        $data = json_decode($response['body'], true);

        if( (! is_array($data)) || (! isset($data['access_token'])) )
        {
            throw new \RuntimeException('Failed to get an access token');
        }

        if( (! is_array($data)) || (! isset($data['access_token'])) )
        {
            throw new \RuntimeException('Failed to get an access token');
        }

        $this->access_token = $data['access_token'];
        //WocPlugin::debug(__METHOD__, 'access_token', $this->access_token );
    }

    /**
     * Redirect the client browser to the OAuth server for authentification and authorization.
     *
     * @return void
     */
    public function oauth_authorize()
    {
        $url_base = $this->get_option_or_fail('oauth_url');
        $url_auth = $this->get_option_or_fail('url_authorize');
        $url = $url_base . $url_auth ;

        $client_id = $this->get_option_or_fail('client_id');

        /**
         * @todo implement scope
         */
        $scope = trim(Settings::get_option('scope', ''));
        /**
         * @todo implement state
         */
        $state = (string) microtime(true);

        $authorizationUrl = $url
        	. '?client_id=' . $client_id
        	. '&scope=' . $scope
        	. '&redirect_uri=' . $this->callback_url()
        	. '&response_type=code'
        	. '&state=' . $state;

        $state_hash = wp_hash( $state, 'nonce' );
        WocPlugin::set_session( self::SESSION_KEY_STATE, $state_hash );

        if ( wp_redirect( $authorizationUrl ) )
        {
            exit ;
        }
        wp_die();
    }


    public function oauth_user_info()
    {        
        try
        {
            $url_base = $this->get_option_or_fail('api_url');
        }
        catch(\InvalidArgumentException $ex )
        {
            $url_base = trim(Settings::get_option('oauth_url'));
        }

        $url_user = $this->get_option_or_fail('url_user');
        $url = $url_base . $url_user ;

        if( ! $this->access_token )
            throw new \RuntimeException('Must have an access token');

        // Default is "true". Set to "false" if wordpress debug.
        $sslverify = (defined('WP_DEBUG') && constant('WP_DEBUG') ? false : true ) ;

        $headers['Authorization'] = 'Bearer ' . $this->access_token ;
        //$headers['OCS-APIRequest'] = 'true';

        $cookies = [];

        $response = wp_remote_get( $url, [
            //'method'      => 'GET',
            'timeout'     => self::HTTP_TIMEOUT,
            'redirection' => self::HTTP_REDIR,
            'httpversion' => '1.0',
            'blocking'    => true,
            'headers'     => $headers,
            'cookies'     => $cookies,
            'sslverify'   => $sslverify
        ]);
        //WocPlugin::debug(__METHOD__, 'response', $response );

        if( is_wp_error($response) )
        {
            //$this->display_error( $response );
            return $response ;
        }

        //WocPlugin::debug(__METHOD__, 'response[response]:', $response['response'] );
        if( $response['response']['code'] != 200 )
        {
            throw new \RuntimeException('Http Get failed: '.json_encode($response['response']) );
        }

        //WocPlugin::debug(__METHOD__,'Response.body',$response['body'] );

        return json_decode($response['body'], true);
    }

}
