<?php
namespace Artefacts\Woc\OAuthProviders ;

use Artefacts\Woc\OAuth ;
use Artefacts\Woc\Settings;
use Artefacts\Woc\WocPlugin;

class LinkedIn extends BaseProvider
{
    public static function optionsConfig( Array $options = null )
    {
        return parent::optionsConfig( [
            '_name'=> 'linkedin',
            'oauth_url' => [
                'readonly' => true,
                'default' => 'https://www.linkedin.com',
            ],
            'url_authorize' => [
                'readonly' => true,
                'default' => '/oauth/v2/authorization',
            ],
            'url_token' => [
                'readonly' => true,
                'default' => '/oauth/v2/accessToken',
            ],
            'scope' => [
                'readonly' => true,
                'default'=>'r_liteprofile,r_emailaddress',
            ],
            'api_url' => [
                'readonly' => true,
                'default' => 'https://api.linkedin.com',
            ],
            'url_user' => [
                'readonly' => true,
                'default' => '/v2/me',
            ],
            'user_login' => [
                'readonly' => true,
                'default' => 'wp_login',
            ],
            'user_name' => [
                'readonly' => true,
                'default' => 'wp_name',
            ],
            'user_email' => [
                'readonly' => true,
                'default' => 'wp_email',
            ],
        ] );
    }

    /**
     * Override the user info api call.
     * LinkedIn need 2 api calls, one for name, another for email
     */
    public function oauth_user_info()
    {
        if( ! $this->access_token )
            throw new \RuntimeException('Must have an access token');

        $url_base = $this->get_option_or_fail('api_url');
        $url_user = $this->get_option_or_fail('url_user');
        $url = $url_base . $url_user ;

        // Default is "true". Set to "false" if wordpress debug.
        $sslverify = (defined('WP_DEBUG') && constant('WP_DEBUG') ? false : true ) ;
        $headers['Authorization'] = 'Bearer ' . $this->access_token ;

        $request_args = [
            //'method'      => 'GET',
            'timeout'     => self::HTTP_TIMEOUT,
            'redirection' => self::HTTP_REDIR,
            'httpversion' => '1.0',
            'blocking'    => true,
            'headers'     => $headers,
            'cookies'     => [],
            'sslverify'   => $sslverify
        ];

        $response = wp_remote_get( $url, $request_args);
        //WocPlugin::debug(__METHOD__, 'response', $response );

        if( is_wp_error($response) )
        {
            return $response ;
        }
        //WocPlugin::debug(__METHOD__, 'response[response]:', $response['response'] );
        if( $response['response']['code'] != 200 )
        {
            throw new \RuntimeException('Http Get failed: '.json_encode($response['response']) );
        }
        //WocPlugin::debug(__METHOD__,'Response.body',$response['body'] );

        $userData = json_decode( $response['body'], true );

        //$url = $url_base . trim(Settings::get_option('url_user'));
        // It's a 404 without the query string
        $url = $url_base . '/v2/emailAddress?q=members&projection=(elements*(handle~))';
        $response = wp_remote_get( $url, $request_args);
        //WocPlugin::debug(__METHOD__, 'response', $response );

        if( is_wp_error($response) )
        {
            return $response ;
        }
        //WocPlugin::debug(__METHOD__, 'response[response]:', $response['response'] );
        if( $response['response']['code'] != 200 )
        {
            throw new \RuntimeException('Http Get failed: '.json_encode($response['response']) );
        }
        //WocPlugin::debug(__METHOD__,'Response.body',$response['body'] );

        $userData = array_merge( $userData, json_decode( $response['body'], true ));

        // Create a fullname item
        $userData['wp_name'] = $userData['localizedFirstName'].' '.$userData['localizedLastName'] ;
        // and some shorten data path
        $userData['wp_email'] = $userData['elements'][0]['handle~']['emailAddress'];
        $userData['wp_login'] = $userData['elements'][0]['handle~']['emailAddress'];

        return $userData ;
    }

}
