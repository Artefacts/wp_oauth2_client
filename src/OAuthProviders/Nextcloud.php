<?php
namespace Artefacts\Woc\OAuthProviders ;

use Artefacts\Woc\OAuth ;
use Artefacts\Woc\Settings;
use Artefacts\Woc\WocPlugin;

class Nextcloud extends BaseProvider
{
    public static function optionsConfig( Array $options = null )
    {
        return parent::optionsConfig( [
            '_name'=> 'nextcloud',
            'url_authorize' => [
                'readonly' => true,
                'default' => '/index.php/apps/oauth2/authorize',
            ],
            'url_token' => [
                'readonly' => true,
                'default' => '/index.php/apps/oauth2/api/v1/token?format=json',
            ],
            'scope' => [
                'readonly' => true,
                'default'=>'',
            ],
            'api_url' => [
                'readonly' => true,
                'default' => '',
            ],
            'url_user' => [
                'readonly' => true,
                'default' => '/ocs/v2.php/cloud/user?format=json',
            ],
            'user_login' => [
                'readonly' => true,
                'default' => 'ocs.data.id',
            ],
            'user_name' => [
                'readonly' => true,
                'default' => 'ocs.data.display-name',
            ],
            'user_email' => [
                'readonly' => true,
                'default' => 'ocs.data.email',
            ],
        ] );
    }

}