<?php
namespace Artefacts\Woc ;

class Settings
{

    protected $option_name ;

    private function __construct()
    {
        $this->option_name = WocPlugin::SLUG.'-options';

        //Caf37Plugin::debug(__METHOD__);
        if( is_blog_admin() )
        {
            add_action( 'admin_init', [$this, 'wp_admin_init'] );
            add_action( 'admin_enqueue_scripts', [$this, 'wp_admin_enqueue_scripts'] );
            add_action( 'admin_menu', [$this, 'wp_admin_menu'] );
        }
        else
        {

        }
    }

    /**
     * @return Settings
     */
    public static function getInstance()
    {
        static $_instance ;
        if( $_instance == null )
            $_instance = new static();
        return $_instance ;
    }

    public static function option_name( )
    {
        return self::getInstance()->option_name ;
    }

    /**
     * A local cache, between method call during a request.
     * 
     * @fixme Find hooks to clear this cache.
     * 
     * @var array $cache_options
     */
    private $cache_options ;

    public static function get_option( $key, $default = null )
    {
        $options =& self::getInstance()->cache_options ;
        if( ! $options )
        {
            self::getInstance()->cache_options = get_option( static::option_name() );
            $options =& self::getInstance()->cache_options ;
            //WocPlugin::debug(__METHOD__, $options);
        }
        if( ! isset( $options[ $key ] ) )
            return $default ;        
        return $options[ $key ];
    }
    
    public function wp_admin_init()
    {
        // https://developer.wordpress.org/reference/functions/register_setting/
		register_setting( $this->option_name, $this->option_name, [
            'show_in_rest' => false ,
            'sanitize_callback' => [$this, 'setting_validation_callback'],
            //'type' => ...
            //'default' => ...
            ] );

    }

    public function setting_validation_callback( $input )
    {

        if( isset($input['create_user']) )
        {
            if( ! $input['create_user'] || $input['create_user'] == '0')
                $input['create_user'] = '0';
            else
                $input['create_user'] = '1';
        }

        if( empty($input['oauth_url']) )
        {
            add_settings_error(
                $this->option_name, 'oauth_url',
                'OAuth server url required',
                'error'
            );
        }
        if( empty($input['user_login']) && empty($input['user_name'] ))
        {
            //$input['user_login'] = null;
            add_settings_error(
                $this->option_name, 'user_login',
                'At least user_name or user_login required',
                'error'
            );
            //$input['user_name'] = null;
            add_settings_error(
                $this->option_name, 'user_name',
                'At least user_name or user_login required',
                'error'
            );
        }

        // Trim all $input
        static::trim_array_values( $input );

        return $input ;
    }

    /**
     * Process recursively all values of $array
     * and replace with it's trimmed version.
     */
    protected static function trim_array_values( Array & $array )
    {
        foreach( $array as $key => & $value )
        {
            if( ! $value )
            {
            }
            else if( is_array($value))
            {
                static::trim_array_values($value);
            }
            else
            {
                $array[$key] = trim($value);
            }
        }
    }

    public function wp_admin_menu()
    {
        add_options_page( 
            $page_title='OAuth Login', $menu_title='OAuth Login',
            $capability='manage_options',
            $menu_slug= WocPlugin::SLUG,
            [$this, 'page_default'],
        );

        /*
        add_menu_page(
            $page_title='OAuth Login', $menu_title='OAuth Login',
            'upload_files',
            $menu_slug= WocPlugin::SLUG,
            [$this, 'page_default'],
            $icon_url = WocPlugin::asset_url().'/menu-icon 26x26.png',
            $position = 34
        );
        add_submenu_page(
            $parent_slug=WocPlugin::SLUG,
            'About', 'About',
            'manage_options',
            WocPlugin::SLUG.'-about',
            [$this, 'page_about']
        );
        */
    }

    /**
     * Add some jQuery UI stuff.
     */
    public function wp_admin_enqueue_scripts()
    {
        /*
        //wp_enqueue_style('wp-jquery-ui-widget');
        wp_enqueue_script( 'jquery-ui-core' );
        wp_enqueue_script( 'jquery-ui-dialog' );
        wp_enqueue_style ( 'wp-jquery-ui-dialog');
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'jquery-ui-core' );
        */

        wp_enqueue_script( 'jquery-ui-accordion' );

        // JQuery UI Style

        $url = WocPlugin::asset_url().'jquery-ui-1.13.0/';
        wp_register_style('jquery-custom-style', $url.'jquery-ui.min.css', [], '1', 'screen'); 
        wp_enqueue_style('jquery-custom-style');
        //wp_register_style('jquery-custom-structure', $url.'jquery-ui.structure.min.css', [], '1', 'screen'); 
        //wp_enqueue_style('jquery-custom-structure');
        //wp_register_style('jquery-custom-theme', $url.'jquery-ui.theme.min.css', [], '1', 'screen'); 
        //wp_enqueue_style('jquery-custom-theme');
        wp_register_style('jquery-custom-custom', $url.'jquery-ui.custom.css', [], '1', 'screen'); 
        wp_enqueue_style('jquery-custom-custom');
    }

    public function page_default()
    {

        if ( ! current_user_can( 'manage_options' ) )
        {
            return;
        }

        // add error/update messages
        //
        // check if the user have submitted the settings
        // WordPress will add the "settings-updated" $_GET parameter to the url
        /*if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            // @see https://developer.wordpress.org/reference/functions/add_settings_error/
            add_settings_error(  WocPlugin::SLUG, 'error', __( 'Settings Saved', 'wporg' ), 'info' );
        }*/
        // show error/update messages
        // @see https://developer.wordpress.org/reference/functions/settings_errors/
        //settings_errors(  WocPlugin::SLUG );

        include_once(WocPlugin::plugin_dir().'templates/options_page_default.php');

    }

    public function page_about()
    {
        include_once( WocPlugin::plugin_dir().'templates/options_page_about.php');
    }
}