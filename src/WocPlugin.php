<?php

namespace Artefacts\Woc ;

require_once(__DIR__.'/Settings.php');
require_once(__DIR__.'/OAuth.php');

use Artefacts\Woc\Settings ;
use Artefacts\Woc\OAuth ;

class WocPlugin
{
    const SLUG = 'woc-oauth';

    public $plugin_dir ;
    public $plugin_dir_url ;
    public $asset_url ;

    /**
     * To choice "cookie" or "session" to store temporary data.
     */
    protected $session_engine ;

    protected function __construct()
    {
        if( $this->ignoreRequest() )
            return ;

        $this->plugin_dir = plugin_dir_path( __DIR__.'/../plugin.php' );
        $this->plugin_dir_url = plugin_dir_url( __DIR__.'/../plugin.php' );
        $this->asset_url = $this->plugin_dir_url.'assets/';

        /*WocPlugin::debug(
            'plugin_dir:', $this->plugin_dir,
            'plugin_dir_url:', $this->plugin_dir_url,
            'asset_url:', $this->asset_url, );
            */

        $this->session_engine = 'cookie';

        //Settings::getInstance();

        // url: /oauth
        // is_admin: false
        // 'REQUEST_URI' => '/oauth'
        // pagenow: index.php

        // option page /wp-admin/options-general.php?page=woc-oauth
        // is_admin True
        // pagenow: options-general.php

        OAuth::getInstance();

    }

    protected function ignoreRequest()
    {
        /*WocPlugin::debug(__METHOD__,
            'is_admin', is_admin() ? 'True' : 'False',
            'pagenow:', $GLOBALS['pagenow'] ?? 'null',
            'plugin_page:', $GLOBALS['plugin_page'] ?? 'null',
            //'_GLOBALS:', $GLOBALS ,
            //'_SERVER:', $_SERVER ,
        );*/

        if( defined('DOING_AJAX') && DOING_AJAX )
        {
            if( isset($_REQUEST['action']) )
            {
                switch($_REQUEST['action'])
                {
                    case 'heartbeat':
                    case 'closed-postboxes':
                    case 'health-check-site-status-result':
                        return true ;
                    case 'oembed-cache':
                        //WocPlugin::debug(__METHOD__,'wp_embed', var_export($GLOBALS['wp_embed'],true));
                        return true ;
                }
            }
            return false ;
        }

        // Not Ajax

        $uri = $_SERVER['REQUEST_URI'];
        //WocPlugin::debug(__METHOD__,'URI', $uri);

        if( strlen($uri)>5 &&  strpos($uri, '.map', -5 ) !== false )
            return true ;

        //WocPlugin::debug(__METHOD__,'REQUEST', var_export($_REQUEST,true));
        //WocPlugin::debug(__METHOD__,'GET', var_export($_GET,true));

        return false ;
    }

    /**
     * @return WocPlugin
     */
    public static function getInstance()
    {
        static $_instance ;
        if( $_instance == null )
            $_instance = new static();
        return $_instance ;
    }

    public static function plugin_dir( )
    {
        return self::getInstance()->plugin_dir ;
    }

    public static function asset_url( )
    {
        return self::getInstance()->asset_url ;
    }

    /**
     * Return the value if $key exists.
     * Start the Php session if not already.
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public static function get_session( $key, $default = null )
    {
        $me = self::getInstance();

        switch( $me->session_engine )
        {
            case 'cookie':
                if( isset($_COOKIE[$key]) )
                    return unserialize(base64_decode($_COOKIE[$key]));
                break;
            case 'session':
                if (session_id() == '' )
                    session_start();
                if( isset($_SESSION[$key]) )
                    return $_SESSION[$key] ;
                break;
        }
        return $default ;
    }

    /**
     * Set the $key value in Php session.
     * engine:
     * - session : Start the Php session if not already.
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public static function set_session( $key, $value = null )
    {
        $me = self::getInstance();

        switch( $me->session_engine )
        {
            case 'cookie':
                $path = '';
                $domain = '';
                if( $value === null )
                    // delete : expire the cookie
                    setcookie( $key, '', time() - 24*3600 , $path, $domain, $secure=true, $httponly=true );
                else
                    setcookie( $key, base64_encode(serialize($value)), time() + 2*60 , $path, $domain, $secure=true, $httponly=true );
                break;
            case 'session':
                if (session_id() == '' )
                    session_start();
                if( $value === null )
                    unset( $_SESSION[$key] );
                else
                    $_SESSION[$key] = $value ;
                break;
        }
    }

    public static function debug( ...$items )
    {
        if( ! defined('WOC_DEBUG') || ! constant('WOC_DEBUG') )
            return ;

        $msg = '' ;
        foreach( $items as $item )
        {
            switch ( gettype($item))
            {
                case 'boolean' :
                    $msg.= ($item ? 'true':'false');
                    break;
                case 'NULL' :
                    $msg.= 'null';
                    break;
                case 'integer' :
                case 'double' :
                case 'float' :
                case 'string' :
                    $msg.= $item ;
                    break;
                default:
                    $msg .= var_export($item,true) ;
            }
            $msg.=' ';
        }
        error_log( $msg );
    }
}
