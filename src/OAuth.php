<?php
namespace Artefacts\Woc ;

use Artefacts\Woc\OAuthProviders\BaseProvider;

require_once( __DIR__.'/OAuthProviders/BaseProvider.php');

class OAuth
{

    protected $access_token ;

    const SESSION_KEY_ERROR = 'woc-sso_error';
    const SESSION_KEY_TEST = 'woc-sso_test';

    const ERROR_STATE_DONTMATCH = 'woc-sso-badstate' ;
    const ERROR_USER_NOTFOUND = 'woc-sso-nouser' ;

    const SHORTCODE_SSOLINK = 'woc_oauth_url' ;
    const SHORTCODE_SSOBUTTON = 'woc_oauth_link' ;

    /**
     * @var \Artefacts\Woc\OAuthProviders\BaseProvider
     */
    protected $_provider ;

    protected function __construct( BaseProvider $provider )
    {
        $this->_provider = $provider ;

        //Caf37Plugin::debug(__METHOD__);
        $this->wordpress_boot();
    }

    protected function wordpress_boot()
    {
        //WocPlugin::debug(__METHOD__,'is_blog_admin', is_blog_admin(), 'is_admin', is_admin() );

        if( is_blog_admin() )
        {
        }
        else
        {
            add_action( 'init', [$this, 'wp_init'] );

            if( is_admin() )
            {
            }
            else
            {
                add_action( 'login_form', [$this, 'wp_login_form_button'] );
                add_filter( 'wp_login_errors', [$this,'wp_login_errors'], 10, 2 );
                add_filter( 'authenticate', [$this,'wp_authenticate_check'], 99, 3);
            }
        }

        add_shortcode( self::SHORTCODE_SSOLINK, [$this,'shortcode_login_link'] );
        add_shortcode( self::SHORTCODE_SSOBUTTON, [$this,'shortcode_login_button'] );

    }

    /**
     * Instanciate the OAuth provider.
     * @return OAuth
     */
    public static function getInstance()
    {
        static $_instance ;
        if( $_instance )
            return $_instance;

        if( isset($_GET[self::provider_select_url_arg()]) )
        {
            $provider = $_GET[self::provider_select_url_arg()];
        }
        else
        {
            $provider = Settings::get_option('provider');
        }

        switch( $provider )
        {
            case 'linkedin':
                require_once( __DIR__.'/OAuthProviders/LinkedIn.php');
                $provider = new OAuthProviders\LinkedIn();
                break;
            case 'nextcloud':
                require_once( __DIR__.'/OAuthProviders/Nextcloud.php');
                $provider = new OAuthProviders\Nextcloud();
                break;
            default:
                $provider = new OAuthProviders\BaseProvider();
        }
        $_instance = new static( $provider );
        return $_instance ;
    }

    public static function oauth_providers()
    {
        return [
            'custom' => 'Custom OAuth',
            'linkedin' => 'LinkedIn',
            'nextcloud' => 'Nextcloud',
        ];
    }

    public static function oauth_provider_name()
    {
        return static::getInstance()->_provider->optionsConfig()['_name'];
    }

    /**
     * Here we process HTTP request.
     */
    public function wp_init()
    {
        /*WocPlugin::debug(__METHOD__,
            'is_admin', is_admin() ? 'True' : 'False',
            'pagenow:', $GLOBALS['pagenow'] ?? 'null',
            'plugin_page:', $GLOBALS['plugin_page'] ?? 'null',
            'REQUEST_URI:', $_SERVER['REQUEST_URI'], 
            '$_REQUEST', $_REQUEST,
            //'_GLOBALS:', $GLOBALS ,
            //'_SERVER:', $_SERVER ,
        );*/

        //
        // Only match CALLBACK URI
        //
        // 'REQUEST_URI' => '/oauth/?woc-oauth-logintest',
        // 'QUERY_STRING' => 'woc-oauth-logintest'

        if( strpos( $_SERVER['REQUEST_URI'], $this->_provider->callback_uri()) != 0 )
            return ;

        if( isset($_GET[ self::authorize_button_url_arg() ]))
        {
            /**
             * Start SSO login to login into Wordpress.
             */

            $this->_provider->oauth_authorize();
        }
        else if( isset($_GET[ self::authorize_test_url_arg() ]))
        {
            /**
             * Start SSO auth to display server user data.
             */
            WocPlugin::set_session( self::SESSION_KEY_TEST, time() );

            $this->_provider->oauth_authorize();
        }
        else if( isset($_REQUEST['code']) && isset($_REQUEST['state']) )
        {
            /**
             * Receive the authorization token,
             * now ask for an Access Token,
             * for api access for retrieving the user attributes (id, email...).
             */

            if( ! $this->_provider->checkAndForgetState( $_REQUEST['state'] ) )
            {
                $this->display_error( new \WP_Error( self::ERROR_STATE_DONTMATCH, 'Protocole state failed' ) );
            }

            $authorization_token = $_REQUEST['code'];

            $result = $this->_provider->oauth_access_token($authorization_token);

            if( is_wp_error($result) )
            {
                $this->display_error( $result );
            }

            $is_test = WocPlugin::get_session( self::SESSION_KEY_TEST );

            if( $is_test )
            {
                WocPlugin::set_session( self::SESSION_KEY_TEST );

                $time = time() - $is_test ;
                $userData = $this->_provider->oauth_user_info() ;

                if( is_wp_error($userData) )
                {
                    $this->display_error( $userData );
                }
                $this->display_test( $userData, $time );
            }
            else
            {
                $this->user_login();
            }
        }

    }

    protected function display_test( Array & $userData, $time )
    {
        ?>
        <h2>OAuth server user attributes:</h2>
        <ul>
        <li>Server: <?php echo Settings::get_option('oauth_url') ?></li>
        <li>Ellapsed time: <?php echo $time ?> secondes</li>
        </ul>
        <pre><?php echo json_encode( $userData, JSON_PRETTY_PRINT ) ?></pre>
        <?php
        wp_die('OAuth flow done.', 'OAuth User data', 200);
    }

    public static function authorize_test_url_arg()
    {
        return WocPlugin::SLUG.'-logintest' ;
    }

    public static function authorize_test_url()
    {
        return static::getInstance()->_provider->callback_url(). '?'.self::authorize_test_url_arg() ;
    }

    public static function provider_select_url_arg()
    {
        return WocPlugin::SLUG.'-provider' ;
    }

    /**
     * @return string The login action query string argument.
     */
    public static function authorize_button_url_arg()
    {
        return WocPlugin::SLUG.'-login' ;
    }

    public static function authorize_button_url()
    {
        return static::getInstance()->_provider->callback_url(). '?'.self::authorize_button_url_arg() ;
    }

    public static function oauth_callback_url()
    {
        return static::getInstance()->_provider->callback_url();
    }

    public static function oauth_options_config()
    {
        return static::getInstance()->_provider->optionsConfig();
    }

    public function button_label()
    {
        return  Settings::get_option('login_form_button_label', 'Login with SSO');
    }

    /**
     * Add SSP login button on the login form.
     * @link https://developer.wordpress.org/reference/hooks/login_form/
     */
    public function wp_login_form_button()
    {
        if( ! Settings::get_option('login_form_button') )
            return ;
        ?>
        <p>
        <a class="button button-primary button-large woc-sso"
            href="<?php echo self::authorize_button_url(); ?>"><?php echo self::button_label(); ?></a>
        </p>
        <?php
    }

    public function shortcode_login_link( $atts )
    {
        $a = shortcode_atts( [
            'test' => false
        ], $atts );
        if( $a['test'] )
            return self::authorize_test_url();
        return self::authorize_button_url();
    }

    public function shortcode_login_button( $atts )
    {
        $a = shortcode_atts( [
            'type'   => 'primary',
            'title'  => 'Login using SSO',
            'class'  => 'woc-sso',
            'target' => '',
            'text'   => $this->button_label(),
            'test' => false
        ], $atts );

        if( $a['test'] )
        {
            $url = self::authorize_test_url();
            $a['target'] = '_blank' ;
        }
        else{
            $url = self::authorize_button_url();
        }

        return '<a class="' . $a['class']
            . '" href="' . $url
            . '" title="' . $a['title']
            . '" target="' . $a['target'] . '">' . $a['text'] . '</a>';
    }

    /**
     * Store error as flash message
     * then redirect to login form
     * that will display this error message.
     * 
     * @see \Artefacts\Woc\OAuth::wp_login_errors()
     */
    protected function display_error( \WP_Error $error )
    {
        WocPlugin::set_session( self::SESSION_KEY_ERROR, $error );
        wp_redirect( wp_login_url() );
        exit();
    }

    /**
     * WP Login form filter.
     * Add stored error for login form screen.
     */
    public function wp_login_errors( \WP_Error $errors, $redirect_to )
    {
        /**
         * @var \WP_Error $flashed_error
         */
        $flashed_error = WocPlugin::get_session( self::SESSION_KEY_ERROR );
        if( ! $flashed_error )
            return $errors ;

        // Add to login form errors
        //$errors->merge_from( $flashed_error );
        // But need to add some "Html" formatting :(
        $errors->add(
            $flashed_error->get_error_code(),
            '<strong>'.__('Error').'</strong> : '.$flashed_error->get_error_message(),
            $flashed_error->get_error_data()
        );

        // forget flashed error
        WocPlugin::set_session( 'woc-sso_error' );

        //WocPlugin::debug(__METHOD__, 'errors:', $errors->get_error_codes());        
        return $errors ;
    }

    /**
     * Retrieve the user info from OAuth server to log the user in Wordpress.
     * If "Create user" option enabled the user will be created in Wordpress.
     * Redirect the user to WP homepage.
     */
    public function user_login()
    {
        $userData = $this->_provider->oauth_user_info() ;

        if( is_wp_error($userData) )
        {
            $this->display_error( $userData );
        }

        /**
         * Fill user data à la Wordpress in $user_info.
         * Extract from $userData with keys from attributes mapping options.
         * 
         * @var array $user_info ;
         */
        $user_info = [];
        $o_user_name = Settings::get_option('user_name') ;
        $o_user_login = Settings::get_option('user_login') ;
        $user_info['first_name'] = OAuth::array_getKeyPath($userData, Settings::get_option('user_name', $o_user_login));
        $user_info['user_login'] = OAuth::array_getKeyPath($userData, Settings::get_option('user_login', $o_user_name));

        $user_info['user_email'] = OAuth::array_getKeyPath($userData, Settings::get_option('user_email'));

        //WocPlugin::debug(__METHOD__,'$user_info',$user_info );

        $user_id = null ;
        $user = get_user_by( 'email', $user_info['user_email'] );

        if( ! $user )
        {
            if( Settings::get_option('create_user') )
            {
                $user_info['user_pass'] =  wp_generate_password(12, false);
                //$user = wp_insert_user($user_info);
                //$user_id = wp_create_user($user_info['user_login'], $user_info['user_pass'], $user_info['user_email']);
                $user_id = wp_insert_user( $user_info);
                if( is_wp_error($user_id) )
                {
                    // probably user name exists ...
                    /*
                    //WocPlugin::debug( __METHOD__, 'wp_create_user() error: '.var_export($user_id,true));
                    wp_send_json_error( $user_id, 403 ) ;
                    //exit();
                    wp_die();*/
                    $this->display_error( $user_id );

                }
                $user  = get_user_by('ID', $user_id );
                $this->user_set_role( $user, $userData );
            }
        }
        else
        {
            $user_id = $user->ID ;
        }
    
        if( ! $user_id )
        {
            $this->display_error( new \WP_Error( self::ERROR_USER_NOTFOUND, 'User dont exists here :(' ) );
        }

        wp_set_current_user($user_id);
        wp_set_auth_cookie($user_id);

        do_action('wp_login', $user->user_login, $user);

        wp_redirect(home_url());
        //wp_die();
        exit();
    
    }
    /**
     * Created user will have this "role" if they match the key/value pair.
     */
    protected function user_set_role( \WP_User $user, Array & $userData )
    {
        $wp_group = null ;
        $opt_groups = Settings::get_option('groups');
        //WocPlugin::debug(__METHOD__,'$opt_groups', $opt_groups );
        if( is_array($opt_groups) ) foreach( $opt_groups as $idx => & $opt_group )
        {
            $user_groups = OAuth::array_getKeyPath( $userData, $opt_group['key'] );
            //WocPlugin::debug(__METHOD__,'opt_group[value]', $opt_group['value'], 'User_groups', $user_groups );
            if( is_array($user_groups) && (in_array( $opt_group['value'], $user_groups) ) )
            {
                $wp_group = $opt_group['wp_role'];
            }
        }
        if( $wp_group )
            $user->set_role( $wp_group );
    }

    /**
     * If option "force_sso" only super user can login normally.
     * 
     * https://developer.wordpress.org/reference/hooks/authenticate/
     * @param null|\WP_User|\WP_Error $user
     * @param string $username
     * @param string $password
     * @return null|\WP_User|\WP_Error A WP_User object is returned if the credentials authenticate a user. WP_Error or null otherwise.
     */
    public function wp_authenticate_check( $user, $username, $password )
    {

        if( ! $user )
        {
            return $user ;
        }
        if( ! ($user instanceof \WP_User) )
        {
            return $user ;
        }
        if( $user->has_cap( 'delete_users' ) )
        {
            // Dont filter super admin
            //is_super_admin( $user->ID )
            return $user ;
        }

        // If option not activated
        if( ! Settings::get_option('force_sso') )
            return $user ;

        return new \WP_Error( WocPlugin::SLUG.'-force-sso', 'You must login with "'.$this->button_label().'" (<em>plugin: '.WocPlugin::SLUG.'</em>)');        
    }

    /**
     * Retrieve a key in a structured data.
     * Key path separator is "." like "k1.k2".
     */
    public static function array_getKeyPath( Array & $array, $keyPath, $default = null )
    {
        if( empty($array) || (! is_array($array)) )
            return $default ;
        $ar =& $array ;
        $value = $default ;
        foreach( explode('.',$keyPath) as $key )
        {
            if( isset($ar[$key]) )
            {
                $value = $ar[$key];
                $ar =& $ar[$key] ;
            }
            else
            {
                return $default ;
            }
        }

        return $value ;
    }
}
