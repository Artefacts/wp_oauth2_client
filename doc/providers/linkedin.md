# LinkedIn

## Configuration

- OAuth credentials: https://www.linkedin.com/developers/
- Create a App
  - An Linkedin page is mandatory
  - Add product “Sign-in with LinkedIn” 

#### Urls

OAuth server settings:
- Request scope: `r_liteprofile,r_emailaddress`
- OAuth url: `https://www.linkedin.com`
- Authorize path: `/oauth/v2/authorization`
- Access token path: `/oauth/v2/accessToken`
- Api url: `https://api.linkedin.com`
- User data path: `/v2/me`

#### Attributes mapping

Auth attributes mapping:
- User login: `wp_login`
- User name: `wp_name`
- User email: `wp_email`

## Tech

Documentation:
- https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow?context=linkedin%2Fcontext&tabs=HTTPS
- https://docs.microsoft.com/fr-fr/linkedin/consumer/integrations/self-serve/sign-in-with-linkedin

LinkedIn is nicely verbose on error:

```
[18-Dec-2021 21:06:44 UTC] Artefacts\Woc\WocPlugin::ignoreRequest REQUEST array (
  'error' => 'invalid_request',
  'error_description' => 'You need to pass the "scope" parameter',
  'state' => '1639861598.8625',
) 
```

error: Scope "r_basicprofile" is not authorized for your application'

### Api /v2/me

```
{
    "localizedLastName": "XxYyZz",
    "profilePicture": {
        "displayImage": "urn:li:digitalmediaAsset:XxYyZz"
    },
    "firstName": {
        "localized": {
            "fr_FR": "XxYyZz"
        },
        "preferredLocale": {
            "country": "FR",
            "language": "fr"
        }
    },
    "lastName": {
        "localized": {
            "fr_FR": "XxYyZz"
        },
        "preferredLocale": {
            "country": "FR",
            "language": "fr"
        }
    },
    "id": "XxYyZz",
    "localizedFirstName": "XxYyZz"
}
```

### Api /v2/emailAddress
```
{
    "handle": "urn:li:emailAddress:3775708763",
    "handle~": {
        "emailAddress": "hsimpson@linkedin.com"
}
```
