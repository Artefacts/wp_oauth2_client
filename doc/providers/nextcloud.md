# Nextcloud

**WARNING** Nextcloud version 20.22 has a bug that makes OAuth to fail login.

It works (tested) with Nextcloud older like 21.0.3 and newer like 20.22.3.

## Security

**WARNING** There is no `scope` associated to the Access Token, so all user account rights are authorized for request.

## It's slow

It could be long, more that 30 secondes. As a "human" we can see message:
> Nous avons détecté plusieurs tentatives de connexion invalides depuis votre adresse IP. C'est pourquoi votre prochaine connexion sera retardée de 30 secondes. 

## Configuration

- documentation https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/oauth2.html
- OAuth credentials: at "Settings / Admibistration / Security" at path `/settings/admin/security`

### Urls

OAuth server settings:
- Request scope: none `scope` with Nextcloud :(
- Authorize path: `/index.php/apps/oauth2/authorize`
- Access token path: `/index.php/apps/oauth2/api/v1/token?format=json`
- User data path: `/ocs/v2.php/cloud/user?format=json`

### Attributes mapping

Wordpress User:
- User role: `ocs.data.groups`
Auth attributes mapping:
- User login: `ocs.data.id`
- User name: `ocs.data.display-name`
- User email: `ocs.data.email`

## API OCS

- https://docs.nextcloud.com/server/latest/developer_manual/client_apis/OCS/ocs-api-overview.html

L'API OCS ne permet pas de récupérer la liste des groupes existants sur le serveur, tous les groupe, pas seulement ceux du user.

### Api /ocs/v2.php/cloud/user

```
{
    "ocs": {
        "meta": {
            "status": "ok",
            "statuscode": 200,
            "message": "OK"
        },
        "data": {
            "enabled": true,
            "storageLocation": "xyzxyz",
            "id": "xyzxyz",
            "lastLogin": 1640038018000,
            "backend": "Database",
            "subadmin": [],
            "quota": {
                "free": 4792540844032,
                "used": 6289364,
                "total": 4792547133396,
                "relative": 0,
                "quota": -3
            },
            "avatarScope": "v2-federated",
            "email": "xyzxyz@xyzxyz.net",
            "emailScope": "v2-federated",
            "additional_mail": [],
            "additional_mailScope": [],
            "displaynameScope": "v2-federated",
            "phone": "",
            "phoneScope": "v2-local",
            "address": "",
            "addressScope": "v2-local",
            "website": "",
            "websiteScope": "v2-local",
            "twitter": "",
            "twitterScope": "v2-local",
            "groups": [
                "xyzxyz"
            ],
            "language": "fr",
            "locale": "fr_FR",
            "notify_email": null,
            "backendCapabilities": {
                "setDisplayName": true,
                "setPassword": true
            },
            "display-name": "xyzxyz"
        }
    }
}
```
