<?php

use Artefacts\Woc\WocPlugin;
use Artefacts\Woc\Settings;
use Artefacts\Woc\OAuth;
?>

<div class="wrap" id="<?php echo Settings::option_name() ?>">

    <h2>Worpress OAuth2 SSO Client</h2>


    <div class="postbox" style="padding: 0 1em;">
        <h3>Active configuration</h3>
        <p>
            The button "<strong><?php echo Settings::get_option('login_form_button_label') ?></strong>"
            will use "<strong><?php echo Settings::get_option('oauth_url') ?></strong>" to login into this Wordpress.
        </p>
        <p>
            If the remote <strong>user email</strong> is not found here,
            <?php if (Settings::get_option('create_user')) { ?>
                a Wordpress user is created
            <?php } else { ?>
                an error is displayed
            <?php } ?>
            (<em><?php echo Settings::option_name() ?>: create_user</em>).
        </p>
        <p>
            Wordpress normal user <strong><?php echo (Settings::get_option('force_sso') ? 'must use SSO for login' : 'can login via Wordpress or SSO') ?></strong>
            (<em><?php echo Settings::option_name() ?>: force_sso</em>).
        </p>
        <p>
            Wordpress anonymous user <strong><?php echo (get_option('users_can_register') ? 'can register' : 'cannot register') ?></strong>
            (<em>wp_option: users_can_register</em>).
        </p>
    </div>

    <form method="post" action="options.php">
        <?php settings_fields(Settings::option_name()); ?>

        <div class="accordion">

            <h3>Wordpress front</h3>
            <div>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">SSO link label</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[login_form_button_label]" min="10" value="<?php echo Settings::get_option('login_form_button_label') ?>" />
                            <p class="description">The label used for the Wordpress login form SSO button or has default for shortcode .</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Button in login form</th>
                        <td>
                            <input type="hidden" name="<?php echo Settings::option_name() ?>[login_form_button]" value="0" />
                            <input type="checkbox" name="<?php echo Settings::option_name() ?>[login_form_button]" value="1" <?php if (Settings::get_option('login_form_button', '1')) { ?> checked="checked" <?php } ?> /> On
                            <p class="description">
                                Display the SSO button on Wordpress login form.
                            </p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Shortcodes</th>
                        <td>
                            <p>The shortcode "<strong>[woc_oauth_url]</strong>" to get the sso login url, "<strong>[woc_oauth_link]</strong>" to get the html link (<em>html "a"</em>).
                            </p>
                            <p class="description">
                                Display a OAuth Sso login action where you need it.
                            </p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Login url</th>
                        <td>
                            <p>The login url is "<strong><?php echo OAuth::authorize_button_url() ?></strong>".
                            </p>
                            <p class="description">
                                This link will launch the Sso login.
                            </p>
                        </td>
                    </tr>
                </table>
                <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Mettre à jour"></p>
            </div>


            <h3>Wordpress users</h3>
            <div>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">Force SSO login</th>
                        <td>
                            <input type="hidden" name="<?php echo Settings::option_name() ?>[force_sso]" value="0" />
                            <input type="checkbox" id="option-force_sso" name="<?php echo Settings::option_name() ?>[force_sso]" value="1" <?php if (Settings::get_option('force_sso', '1')) { ?> checked="checked" <?php } ?> />
                            <label for="option-force_sso">On</label>
                            <p class="description">
                                Only super user (admin) can login without SSO, other users must login via SSO.
                            </p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Create user</th>
                        <td>
                            <input type="hidden" name="<?php echo Settings::option_name() ?>[create_user]" value="0" />
                            <input type="checkbox" id="option-create_user" name="<?php echo Settings::option_name() ?>[create_user]" value="1" <?php if (Settings::get_option('create_user', '1')) { ?> checked="checked" <?php } ?> />
                            <label for="option-create_user">On</label>
                            <p class="description">
                                Automatic creation of Wordpress user.
                            </p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">User role</th>
                        <td>
                            <p class="description">
                                By default a new created user will have the "role" like it's set in Wordpress option.<br />
                                You can add override this behavior by defining a "key/value" pair to map "groups" from the Oauth server to Wordpress's role.
                                If user doead not match the rule, the default Wordpress behavior will be applied.
                            </p>
                            <?php
                            $groups = Settings::get_option('groups');
                            if (!is_array($groups) || (count($groups) == 0)) {
                                $groups = [['key' => '', 'value' => '', 'wp_role' => '']];
                            }
                            foreach ($groups as $idx => $group) {
                            ?>
                                <label>Key path</label>
                                <input type="text" name="<?php echo Settings::option_name() . '[groups][' . $idx . '][key]'; ?>" min="" value="<?php echo $group['key']; ?>" />
                                <label>Value</label>
                                <input type="text" name="<?php echo Settings::option_name() . '[groups][' . $idx . '][value]'; ?>" min="" value="<?php echo $group['value']; ?>" />
                                <label>Role</label>
                                <select name="<?php echo Settings::option_name() . '[groups][' . $idx . '][wp_role]'; ?>">
                                    <?php wp_dropdown_roles($group['wp_role']) ?>
                                </select>
                            <?php
                            }
                            ?>
                            <p class="description">
                                Created user will have this "role" if they match the key/value pair.
                            </p>

                        </td>
                    </tr>
                </table>

                <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Mettre à jour"></p>
            </div>


            <h3>OAuth server settings</h3>
            <div>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">Provider</th>
                        <td>
                            <select name="<?php echo Settings::option_name() ?>[provider]">
                                <option value="">Select a provider</option>
                                <?php foreach( OAuth::oauth_providers() as $key => $label) { ?>
                                <option value="<?php echo $key ?>"
                                    <?php if( $key == OAuth::oauth_provider_name() ) { ?>
                                        selected="selected"
                                    <?php } ?>
                                    >
                                <?php echo $label ?>
                                <?php } ?>
                            </select>
                            <p class="description">Select a predefined OAuth server, or use the Custom one.</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Here callback url</th>
                        <td>
                            <?php echo OAuth::oauth_callback_url() ?>
                            <p class="description">This is the "redirect url" to set in the OAuth server.</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Client ID</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[client_id]" min="10" value="<?php echo Settings::get_option('client_id') ?>" />
                            <p class="description">From the OAuth server.</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Client Secret</th>
                        <td>
                            <input type="password" name="<?php echo Settings::option_name() ?>[client_secret]" min="10" value="<?php echo Settings::get_option('client_secret'); ?>" />
                            <p class="description">From the OAuth server.</p>
                        </td>
                    </tr>
                </table>

                <h4>OAuth and Api url</h4>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">Request scope</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[scope]" min="10" value="<?php echo Settings::get_option('scope') ?>" />
                            <p class="description">From the OAuth API documentation.</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">OAuth url</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[oauth_url]" min="10" value="<?php echo Settings::get_option('oauth_url') ?>" />
                            <p class="description">The HTTP/S server url for OAuth : https://mycloud.internet.net</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Authorize path</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[url_authorize]" min="5" value="<?php echo Settings::get_option('url_authorize'); ?>" />
                            <p class="description">Nextcloud: /index.php/apps/oauth2/authorize</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Access token path</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[url_token]" min="5" value="<?php echo Settings::get_option('url_token'); ?>" />
                            <p class="description">Nextcloud: /index.php/apps/oauth2/api/v1/token?format=json</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Api url</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[api_url]" min="10" value="<?php echo Settings::get_option('api_url') ?>" />
                            <p class="description">The HTTP/S server url API : https://api.internet.net. If not set the OAuth server url will be used.</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">User data path</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[url_user]" min="5" value="<?php echo Settings::get_option('url_user'); ?>" />
                            <p class="description">Nextcloud: /ocs/v2.php/cloud/user?format=json</p>
                        </td>
                    </tr>
                </table>

                <h4>Auth attributes mapping</h4>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">User login</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[user_login]" min="" value="<?php echo Settings::get_option('user_login') ?>" />
                            <p class="description">If empty "user name" will be used.</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">User name</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[user_name]" min="" value="<?php echo Settings::get_option('user_name') ?>" />
                            <p class="description">If empty "user login" will be used.</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">User email</th>
                        <td>
                            <input type="text" name="<?php echo Settings::option_name() ?>[user_email]" min="" value="<?php echo Settings::get_option('user_email'); ?>" />
                            <p class="description">Email is the unique key to find a Wordpress's user.</p>
                        </td>
                    </tr>
                </table>

                <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Mettre à jour"></p>
            </div>

        </div>

        <div class="postbox" style="padding: 0 1em; margin-top: 1em;">
            <h3>Test configuration</h3>
            <p>
                Process the SSO flow to display OAuth server user data :
                <?php echo do_shortcode('[' . OAuth::SHORTCODE_SSOBUTTON . ' test=1]'); ?><br />
                - without interfering with current logged Wordpress user.<br />
                - you will see user information of authenticated remote user.
            </p>
        </div>

        </form>
</div><!-- .wrap -->

<script>
    jQuery(document).ready(function($)
    {
        //var optionsConfig = oauth_options_config
        var $woc = $('#<?php echo Settings::option_name() ?>');
        var $form = $('form', $woc);
        var optionsName = '<?php echo Settings::option_name() ?>' ;
        var optionsConfig = <?php echo json_encode( OAuth::oauth_options_config() ); ?> ;
        //var optionsConfig = [];

        console.debug('optionsConfig', optionsConfig);

        /*var icons = {
            header: "ui-icon-circle-arrow-e",
            activeHeader: "ui-icon-circle-arrow-s"
        };*/
        function enableNav()
        {
            $(".accordion", $woc).accordion({
                collapsible: true,
                //icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" },
                //icons: { "header": "ui-icon-circle-arrow-e", "activeHeader": "ui-icon-circle-arrow-s" },
                heightStyle: "content",
            });
        }

        function applyOptionsConfig()
        {
            // Only process form fields name that match this regex.
            // Ex field name like : "woc-oauth-options[user_email]"
            var optionKeyRe = new RegExp( '^'+optionsName+'\\[(.*)\\]$' );

            $('input,select',$form).each(function(idx,el)
            {
                //console.log( 'name:', el.getAttribute('name') );
                var $el = $(el);
                var match = optionKeyRe.exec( el.getAttribute('name') );
                if( ! match )
                    return ;
                var optName = match[1];
                console.log( "Found: ", optName, optionsConfig[ optName ] );
                var optCfg = optionsConfig[ optName ] ? optionsConfig[ optName ] : {};

                if( optCfg['readonly'] && optCfg['readonly'] == true )
                {
                    //$el.prop('disabled',true);
                    $el.prop('readonly',true);
                    if( optCfg['default'] !== undefined )
                        $el.val( optCfg['default'] );
                }
                else
                {
                    //$el.prop('disabled',false);
                    $el.prop('readonly',false);
                }

            });

        }

        enableNav();
        applyOptionsConfig();

    });
</script>


